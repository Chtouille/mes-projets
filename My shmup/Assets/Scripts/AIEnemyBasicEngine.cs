﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIEnemyBasicEngine : MonoBehaviour
{
    private Vector2 enemymovement;
    private EnemyBehavior enemybehavior;
    private float maxspeed;

    // Start is called before the first frame update
    void Start()
    {
        enemybehavior = GetComponent<EnemyBehavior>();
        maxspeed = enemybehavior.maxSpeed;
        enemymovement =  new Vector2 (-1f * maxspeed, 0f); 
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(enemymovement*Time.deltaTime);
        if (transform.position[0] < -2)
        {
            Destroy(gameObject);
            int scorevalue = int.Parse(enemybehavior.txtscorevalue.GetComponent<Text>().text) - 3;
            enemybehavior.txtscorevalue.GetComponent<Text>().text = scorevalue + "";
        }
    }
}
