﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletGun : MonoBehaviour
{
    [SerializeField]
    private GameObject simplebullet;

    [SerializeField]
    private GameObject spiralebullet;

    private InputController inputcontroller;
    private bool isFiring;
    private float elapsedenergy;
    private float elapsedfire;
    private float cooldown;
    private Slider energyslider;
    private float energy;
    private bool canshoot;
    private int guntype;
    private int count;

    void Start()
    {
        inputcontroller = GetComponent<InputController>();
        elapsedenergy = 0.2f;
        elapsedfire = 0f;
        energy = 0f;
        cooldown = 0.2f;
        canshoot = true;
        guntype = 1;
        count = 0;
        energyslider = GameObject.FindGameObjectWithTag("energyslider").GetComponent<Slider>();
    }

    void OnEnable()
    {
        EventManager.OnPressTab += ChangeGun;
    }

    void OnDisable()
    {
        EventManager.OnPressTab -= ChangeGun;
    }

    void Update()
    {
        elapsedfire += Time.deltaTime;
        energyslider.value = energy;
        isFiring = inputcontroller.isfiring;

        if (elapsedfire >= cooldown)
        {
            if ((isFiring == true) && (energy <= 1f) && (canshoot == true))
            {
                Fire();
                ConsumeEnergy();
                elapsedfire = 0f;

            }

            else { EnergyBehavior(); }
        }


 
        }
    
    public void EnergyBehavior()
    {
        elapsedenergy += Time.deltaTime;
        if (elapsedenergy >= cooldown)
        {

        if ((energy <= 2f) && (canshoot == true) && (elapsedenergy >= cooldown))
            {
                ReloadEnergy();
            }

            else if (elapsedenergy >= cooldown)
            {
                FullyReloadEnergy();
            }
            elapsedenergy = 0f;
        }

    }
    public void Fire()
    {
        if (guntype == 1)
        {
            Instantiate(simplebullet, transform.position + new Vector3(1.2f, -0.6f, 0f), Quaternion.identity);
        }
        if (guntype == 2)
        {
            Instantiate(simplebullet, transform.position + new Vector3(1.2f, -0.6f, 0f), new Quaternion(0f, 0f, -0.1305262f, 0.9914449f));
            Instantiate(simplebullet, transform.position + new Vector3(1.2f, -0.3f, 0f), new Quaternion (0f,0f, 0.1305262f, 0.9914449f));
        }
        if (guntype == 3)
        {
            Instantiate(spiralebullet, transform.position + new Vector3(1.2f, -0.6f, 0f), Quaternion.identity);
        }
    }



    public void ChangeGun() //bidouillage ici mais ça fonctionne : en fait ChangeGun est appelé plusieurs fois 
                            //pour une seule pression de tab donc j'ai ajouté un count pour réguler tout ça
    {

        
        if (count == 3)
        {
            if (guntype == 1)
            {
                guntype = 2;

            }
            else if (guntype == 2)
            {
                guntype = 1;
 
            }
            else if (guntype == 3)
            {
                guntype = 1;
            }
            count = 0;
        }
        else
        {
            count += 1;
        }

    }

    public void ConsumeEnergy() //Plus mon tir est puissant, plus il consomme d'énergie !
    {
        if (guntype == 1)
        {
            energy += 8f * Time.deltaTime;
        }
        if (guntype == 2)
        {
            energy += 15f * Time.deltaTime;
        }
    }
    public void ReloadEnergy()
    {
        if ((energy < 1f)&&(energy>0f))
        {
            energy -= 8f * Time.deltaTime;
        }
        else if (energy <= 0f)
        {
            energy = 0f;
        }
        else
        {
            canshoot = false;
        }
    }
    public void FullyReloadEnergy()
    {
        if (energy >= 0f)
        {
            energy -= 6f * Time.deltaTime;
        }
        else
        {
            energy = 0f;
            canshoot = true;
        }

    }
}
