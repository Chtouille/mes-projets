﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAvatar : BaseAvatar
{
    private Slider healthslider;

    void Start()
    {
        MaxSpeed = 10f;
        MaxHealth = 3f;
        Health = 3f;
        healthslider = GameObject.FindGameObjectWithTag("healthslider").GetComponent<Slider>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("EnemyBullet"))
        {
            TakeDamage(1f); //à modifier
            healthslider.value += 1f;

        }
    }
}