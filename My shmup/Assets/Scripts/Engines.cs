﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Engines : MonoBehaviour
{
    private InputController inputcontroller;
    private PlayerAvatar playeravatar;
    private Vector2 speed;
    private Vector2 position;
    private float maxspeed;

    // Start is called before the first frame update
    void Start()
    {
        playeravatar = GetComponent<PlayerAvatar>();
        inputcontroller = GetComponent<InputController>();
        transform.position = new Vector2 (2.5f, 0f);
        maxspeed = playeravatar.maxSpeed;

    }

    // Update is called once per frame
    void Update()
    {
       
        position = transform.position;
        speed = inputcontroller.speed * Time.deltaTime * maxspeed; 
         if ((position[0] < 2)&&(speed[0]<0))
            {
            speed[0] = 0;
            if ((position[1] < -9) && (speed[1] < 0))
            {
                speed[1] = 0;
            }
            else if ((position[1] > 9) && (speed[1] > 0))
            {
                speed[1] = 0;
            }
                transform.Translate(speed);
        }
         else if ((position[0] > 15) && (speed[0] > 0))
        {
            speed[0] = 0;
            if ((position[1] < -9) && (speed[1] < 0))
            {
                speed[1] = 0;
            }
            else if ((position[1] > 9) && (speed[1] > 0))
            {
                speed[1] = 0;
            }
            transform.Translate(speed);
        }
         else if ((position[1] < -9) && (speed[1] < 0))
        {
            speed[1] = 0;
            transform.Translate(speed);
        }
         else if ((position[1] > 9) && (speed[1] > 0))
        {
            speed[1] = 0;
            transform.Translate(speed);
        }
         else
        {
            transform.Translate(speed);

        }
      } 
  }


