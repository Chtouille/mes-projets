﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    public delegate void TabAction();
    public static event TabAction OnPressTab;

    void OnGUI()
    {
        if (Input.GetKeyDown("tab"))
        {
            if (OnPressTab != null)
            {
                OnPressTab();
            }
        }
    }
}
