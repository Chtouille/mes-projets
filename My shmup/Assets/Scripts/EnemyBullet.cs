﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : Bullet
{
    // Start is called before the first frame update
    void Start()
    {
        Init(1f, new Vector2(-25, 0), Quaternion.identity/*, BulletType.EnumBulletType.ENEMYBULLET*/);
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePosition(this.Speed);
    }
}