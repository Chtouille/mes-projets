﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : BaseAvatar
{
    void Start()
    {
        MaxSpeed = 2f;
        MaxHealth = 1f;
        Health = 1f;
        txtscorevalue = GameObject.FindGameObjectWithTag("textscorevalue");
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("PlayerBullet"))
        {
            TakeDamage(1f); //à modifier
        }
    }
}
