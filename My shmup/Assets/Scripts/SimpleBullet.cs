﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBullet : Bullet
{
    // Start is called before the first frame update
    void Start()
    {
        Init(1f, new Vector2(30, 0), Quaternion.identity/*, BulletType.EnumBulletType.SIMPLEPLAYERBULLET*/);
    }

    // Update is called once per frame
    void Update()
    {
        UpdatePosition(this.Speed);
    }
}
