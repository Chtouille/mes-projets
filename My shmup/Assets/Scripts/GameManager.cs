﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Singleton { get; private set; }

    [SerializeField]
    private GameObject player;

    [SerializeField]
    private GameObject enemy;

    [SerializeField]
    private GameObject enemy1;

    private float elapsed;
    private float elapsed2;
    private float cooldown;
    private float cooldown2;
    

    private void Awake()
    {
        if (Singleton == null)
        {
            Singleton = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        Instantiate(player, transform.position + new Vector3(2.5f, 0f, 0f), Quaternion.identity);
        cooldown = 3f;
        cooldown2 = 4f;
    }

    void Update()
    {
        elapsed2 += Time.deltaTime;
        elapsed += Time.deltaTime;
        if (elapsed >= cooldown)
        {
            Instantiate(enemy, transform.position + new Vector3(40f, Random.Range(-9f, 9f), 0f), Quaternion.identity);
            elapsed = 0f;

        }
        if (elapsed2 >= cooldown2)
        {
            Instantiate(enemy1, transform.position + new Vector3(40f, Random.Range(-3f, 3f), 0f), Quaternion.identity);
            elapsed2 = 0f;
        }
    }
}
