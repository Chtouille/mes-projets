﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    private float Damage;
    public Vector2 Speed;
    private Vector2 Position;
    private Quaternion quaternion;
    private GameObject player;

    public void Init(float damage, Vector2 speed, Quaternion _quaternion/*, BulletType.EnumBulletType enumbullettype*/)
    {
        this.Damage = damage;
        this.quaternion = _quaternion;
        this.Speed[0] = speed[0] ;
        this.Speed[1] = speed[1] ;
       /* this.BulletType.EnumBulletType = BulletType.EnumBulletType.enumbullettype;*/
    }

    public void UpdatePosition(Vector2 speed)
    {
        Position = transform.position;
        transform.Translate(speed*Time.deltaTime);
        if ((Position[0] < -15)|| (Position[0] > 50))
        {
            Destroy(gameObject);
        }
    }
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }

}
