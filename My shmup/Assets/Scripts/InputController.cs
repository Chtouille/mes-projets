﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public Vector2 speed;
    public bool isfiring;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float y_translation = Input.GetAxisRaw("Vertical");  
        float x_translation = Input.GetAxisRaw("Horizontal");
        float isFiring = Input.GetAxisRaw("Fire1");
        if (isFiring == 1f)
        {
            isfiring = true;
        }
        else { isfiring = false; }
        speed = new Vector2(x_translation, y_translation);

    }
}
