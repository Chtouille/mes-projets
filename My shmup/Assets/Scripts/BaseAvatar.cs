﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class BaseAvatar : MonoBehaviour
{

    public float maxSpeed;


    public float health; 

    public float maxHealth; 

    public GameObject txtscorevalue;

    public float MaxSpeed
    {
        get
        {
            return maxSpeed;
        }
        set
        {
            maxSpeed = value;
        }
    }

    public float Health
    {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }

    public float MaxHealth
    {
        get
        {
            return maxHealth;
        }
        set
        {
            maxHealth = value;
        }
    }

    public void TakeDamage(float damage)
    {
        this.Health -= damage; 
        if (this.Health <= 0)
        {
            Die();
        }
    }

    public void Die()
    {
        Destroy(gameObject);
        if (gameObject.tag == "Enemy")
        {
            int scorevalue = int.Parse(txtscorevalue.GetComponent<Text>().text) + 1;
            txtscorevalue.GetComponent<Text>().text = scorevalue + "";
        }
    }

  
}
