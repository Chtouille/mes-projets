﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AIEnemyNonBasicEngine : MonoBehaviour
{
    private Vector2 enemymovement;
    private EnemyBehavior enemybehavior;
    private float maxspeed;
    private float elapsed;
    private int positifnegatif;

    // Start is called before the first frame update
    void Start()
    {
        elapsed = 0f;
        positifnegatif = 1;
        enemybehavior = GetComponent<EnemyBehavior>();
        maxspeed = enemybehavior.maxSpeed;
        enemymovement = new Vector2(-1f * maxspeed, maxspeed);
    }

    // Update is called once per frame
    void Update()
    {
        elapsed += Time.deltaTime;
        if (elapsed >= 3f)
        {
            positifnegatif *= -1;
            elapsed = 0f;
        }
        enemymovement = new Vector2(-1f*maxspeed, maxspeed*positifnegatif);
        transform.Translate(enemymovement * Time.deltaTime);
        if (transform.position[0] < -2)
        {
            Destroy(gameObject);
            int scorevalue = int.Parse(enemybehavior.txtscorevalue.GetComponent<Text>().text) - 3;
            enemybehavior.txtscorevalue.GetComponent<Text>().text = scorevalue + "";
        }
    }
}


