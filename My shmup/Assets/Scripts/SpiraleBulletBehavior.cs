﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiraleBulletBehavior : Bullet
{
    // Start is called before the first frame update
    private float count;
    private float count2;
    private int positifnegatif;

    void Start()
    {
        Init(1f, new Vector2(30, 0), Quaternion.identity/*, BulletType.EnumBulletType.SPIRALEPLAYERBULLET*/);
        count = 0f;
        positifnegatif = 1;
    }

    // Update is called once per frame
    void Update()
    {
        count += 3f*Time.deltaTime;
        count2 += Time.deltaTime;
        if (count2 >= 0.5f)
        {
            positifnegatif *= -1;
            count2 = 0f;

        }
        this.Speed[0] = positifnegatif*count*count2;
        this.Speed[1] = positifnegatif*count*count2;
        UpdatePosition(this.Speed);
    }
}
