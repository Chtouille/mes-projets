﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBasicBulletGun : MonoBehaviour
{
    [SerializeField]
    private GameObject enemybullet; 

    private float elapsed;
    private float cooldown;

    void Start()
    {
        elapsed = 0f;
        cooldown = 1f;
    }
    void Update()
    {
        elapsed += Time.deltaTime;
        if ((elapsed >= cooldown))
        {
            Fire();
            elapsed = 0f;

        }
    }

    public void Fire()
    {
        Instantiate(enemybullet, transform.position + new Vector3(-0.8f, -0.25f, 0f), Quaternion.identity);
    }
}
