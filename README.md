Map : partie sur laquelle j'ai travaillé pour un projet JavaScript en groupe qui a deux ans maintenant, il s'agissait de
      générer une map aléatoire sous forme de grille où chaque case a un biome associé. 
      
My shmup: un shoot'em up, c'est le premier projet que j'ai réalisé sur Unity en début d'année. 
          Malheureusement à cause d'un soucis de version je n'arrive plus à le faire fonctionner, mais vous pouvez toujours jeter un oeil aux scripts !
          
Platformer : projet sur lequel on était deux à travailler, sur Unity également. L'idée derrière ce projet était de faire fonctionner un moteur physique 
             sans utiliser les outils fournis par Unity, le script CharacterController correspond à ma tentative.