﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserDetection : MonoBehaviour
{

    private GameObject player;

    private GameObject laserLight;

    [SerializeField]
    private float speed = 5;

    [SerializeField]
    private bool isY = false;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Alert !");
        player.GetComponent<CharacterBehaviour>().Death();
        //if (player.GetComponent<PlayerController>().hasStarted)
        //{
        //   player.GetComponent<PlayerController>().isArrested = true;
        //}
    }
}
