﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptMemo : MonoBehaviour
{
    #region déclaration des variables

    private bool estDansLesAirs;
    public bool EstDansLesAirs { get => estDansLesAirs; set => estDansLesAirs = value; }

    private bool graviteFaible;
    public bool GraviteFaible { get => graviteFaible; set => graviteFaible = value; }

    private float vitesseGravite { get; set; }
    public float VitesseGravite { get => vitesseGravite; set => vitesseGravite = value; }

    private bool estEnTrainDeCourir;
    public bool EstEnTrainDeCourir { get => estEnTrainDeCourir; set => estEnTrainDeCourir = value; }

    private bool estContreUnMurGauche;
    public bool EstContreUnMurGauche { get => estContreUnMurGauche; set => estContreUnMurGauche = value; }

    private bool estContreUnMurDroite;
    public bool EstContreUnMurDroite { get => estContreUnMurDroite; set => estContreUnMurDroite = value; }

    private bool estEnDessousDUnMur;
    public bool EstEnDessousDUnMur { get => estEnDessousDUnMur; set => estEnDessousDUnMur = value; }

    //vrai si ((GravityManager.EstDansLesAirs) et déplacement suivant y négatif)
    private bool estEnTrainDeTomber;
    public bool EstEnTrainDeTomber { get => estEnTrainDeTomber; set => estEnTrainDeTomber = value; }

    private Vector2 deplacement;
    public Vector2 Deplacement { get => deplacement; set => deplacement = value; }

    private Rigidbody2D rigidBody2D;
    private float inputDeplacementJoueur = 0f;
    private float inputSaut = 0f;
    private float deplacementJoueurX = 0f;
    private float deplacementJoueurY = 0f;
    private float graviteValeur = 10.0f;
    private ContactFilter2D contactFilter;
    private RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    private List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    [SerializeField]
    private float graviteModifier = 10.0f;

    [SerializeField]
    private float GraviteFaibleModifier = 3.0f;

    [SerializeField]
    private float maxGravite = 20.0f;

    [SerializeField]
    private float deplacementMinimal = 0.001f;

    [SerializeField]
    private float distanceSecurite = 0.1f;

    [SerializeField]
    private float vitesseMaxMarche = 5f;

    [SerializeField]
    private float accelerationMarche = 15f;

    [SerializeField]
    private float decelerationMarche = 30f;

    [SerializeField]
    private float impulsionSautMarche = 10.0f;

    [SerializeField]
    private float vitesseMaxCourse = 10f;

    [SerializeField]
    private float accelerationCourse = 30f;

    [SerializeField]
    private float decelerationCourse = 40f;

    [SerializeField]
    private float impulsionSautCourse = 15.0f;

    #endregion

    void Awake()
    {
        EstEnTrainDeCourir = false;
        EstContreUnMurGauche = false;
        EstContreUnMurDroite = false;
        EstEnTrainDeTomber = false;
        EstEnDessousDUnMur = false;
        EstDansLesAirs = false;
        GraviteFaible = false;
        EstDansLesAirs = true;
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        Debug.Log("estcontreunmurGAUCHE : " + EstContreUnMurGauche);
        Debug.Log("estcontreunmurDROITE : " + EstContreUnMurDroite);
        Debug.Log("estenDESSOUSdunmur : " + EstEnDessousDUnMur);
        Debug.Log("estentraindeTOMBER : " + EstEnTrainDeTomber);
        Debug.Log("Airs : " + gravityManager.EstDansLesAirs);

        SetEtat();
        SetDeplacementJoueurX(inputDeplacementJoueur, deplacementJoueurX, EstContreUnMurGauche, EstContreUnMurDroite, EstEnTrainDeCourir);
        SetDeplacementJoueurY(gravityManager.VitesseGravite, inputSaut, deplacementJoueurY, EstEnDessousDUnMur, gravityManager.EstDansLesAirs, EstEnTrainDeCourir);
        Deplacement = new Vector2(deplacementJoueurX, deplacementJoueurY);
        GestionCollisions(Deplacement);
        DeplacerJoueur(Deplacement);
    }

    void SetEtat()
    {
        inputSaut = Input.GetAxis("Jump");
        inputDeplacementJoueur = Input.GetAxisRaw("Horizontal");
        EstContreUnMurDroite = EstContreUnMurDroite;
        EstContreUnMurGauche = EstContreUnMurGauche;
        EstEnDessousDUnMur = EstEnDessousDUnMur;

        if (Input.GetAxisRaw("Course") == 1)
        {
            EstEnTrainDeCourir = true;
        }
        else
        {
            EstEnTrainDeCourir = false;
        }

        if ((gravityManager.EstDansLesAirs) && (deplacementJoueurY < 0f))
        {
            EstEnTrainDeTomber = true;
        }
        else
        {
            EstEnTrainDeTomber = false;
        }
    }

    void GestionCollisions(Vector2 deplacement)
    {
        float distance = deplacement.magnitude;
        if (distance > deplacementMinimal)
        {
            int count = rigidBody2D.Cast(deplacement, contactFilter, hitBuffer, distance + distanceSecurite);
            hitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            for (int i = 0; i < hitBufferList.Count; i++)
            {
                if (hitBufferList[i].distance <= distanceSecurite)
                {
                    Vector2 currentNormal = hitBufferList[i].normal;

                    if (currentNormal.y == 1)
                    {
                        EstDansLesAirs = false;
                    }
                    else
                    {
                        EstDansLesAirs = true;
                    }
                    if (currentNormal.y == -1)
                    {
                        EstEnDessousDUnMur = true;
                    }
                    else
                    {
                        EstEnDessousDUnMur = false;
                    }
                    if (currentNormal.x == 1)
                    {
                        EstContreUnMurGauche = true;
                    }
                    else
                    {
                        EstContreUnMurGauche = false;
                    }
                    if (currentNormal.x == -1)
                    {
                        EstContreUnMurDroite = true;
                    }
                    else
                    {
                        EstContreUnMurDroite = false;
                    }
                }


            }


        }
    }

    //prend un compte le déplacement à la frame d'avant, les inputs courants et établis le déplacement courant
    void SetDeplacementJoueurX(float inputDeplacementJoueur, float _deplacementJoueurX, bool estContreUnMurGauche, bool estContreUnMurDroite, bool estEnTrainDeCourir)
    {
        float vitesseMax = vitesseMaxMarche;
        float acceleration = accelerationMarche;
        float deceleration = decelerationMarche;

        if (estEnTrainDeCourir)
        {
            vitesseMax = vitesseMaxCourse;
            acceleration = accelerationCourse;
            deceleration = decelerationCourse;
        }

        if ((estContreUnMurGauche) && (inputDeplacementJoueur <= 0))
        {
            deplacementJoueurX = 0f;
        }

        else if ((estContreUnMurDroite) && (inputDeplacementJoueur >= 0))
        {
            deplacementJoueurX = 0f;
        }

        else if ((_deplacementJoueurX == 0f) && (inputDeplacementJoueur == 0f))
        {
            deplacementJoueurX = 0f;
        }

        else if (inputDeplacementJoueur != 0f)
        {
            if (Mathf.Abs(_deplacementJoueurX) <= vitesseMax)
            {
                deplacementJoueurX = _deplacementJoueurX + inputDeplacementJoueur * acceleration * Time.deltaTime;
            }
            else
            {
                deplacementJoueurX = Mathf.Sign(_deplacementJoueurX) * vitesseMax;
            }
        }

        else if (inputDeplacementJoueur == 0f)
        {
            deplacementJoueurX = _deplacementJoueurX - Mathf.Sign(deplacementJoueurX) * deceleration * Time.deltaTime;
            if (Mathf.Sign(_deplacementJoueurX) != Mathf.Sign(deplacementJoueurX))
            {
                deplacementJoueurX = 0f;
            }
        }
    }

    void SetDeplacementJoueurY(float vitesseGravite, float inputSaut, float _deplacementJoueurY, bool estEnDessousDUnMur, bool estDansLesAirs, bool estEnTrainDeCourir)
    {
        float impulsionSaut = impulsionSautMarche;

        if (estEnTrainDeCourir)
        {
            impulsionSaut = impulsionSautCourse;
        }

        if ((inputSaut == 0f) && (!estDansLesAirs))
        {
            deplacementJoueurY = 0f;
        }

        else if ((inputSaut == 1f) && (!estDansLesAirs))
        {
            deplacementJoueurY = impulsionSaut;
        }

        else if (estDansLesAirs)
        {
            deplacementJoueurY = _deplacementJoueurY + vitesseGravite;
        }

        if (estEnDessousDUnMur)
        {
            deplacementJoueurY = -_deplacementJoueurY;
        }
    }

    public void DeplacerJoueur(Vector2 deplacement)
    {
        rigidBody2D.MovePosition(rigidBody2D.position + deplacement * Time.deltaTime);

    }
}*/