﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//S'il n'y a pas de sol en dessous : gravityManager.EstEnTrainDeTomber = true;
//S'il est contre un mur : characterController.EstContreUnMur = true; characterController.vitesseCouranteHorizontale = 0.0f; et s'il descend gravityManager.GraviteFaible = true sinon rechanger les valeurs

public class ColliderManager : MonoBehaviour
{
    #region déclaration des variables

    private bool estDansLesAirs;
    public bool EstDansLesAirs { get => estDansLesAirs; set => estDansLesAirs = value; }

    private bool estContreUnMurGauche;
    public bool EstContreUnMurGauche { get => estContreUnMurGauche; set => estContreUnMurGauche = value; }

    private bool estContreUnMurDroite;
    public bool EstContreUnMurDroite { get => estContreUnMurDroite; set => estContreUnMurDroite = value; }

    private bool estEnDessousDUnMur;
    public bool EstEnDessousDUnMur { get => estEnDessousDUnMur; set => estEnDessousDUnMur = value; }

    private Rigidbody2D rigidBody2D;
    private GravityManager gravityManager;
    private CharacterController characterController;
    private ContactFilter2D contactFilter;
    private RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    private List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    [SerializeField]
    private float deplacementMinimal = 0.001f;

    [SerializeField]
    private float distanceSecurite = 0.5f;
    #endregion

    void Awake()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
        gravityManager = GetComponent<GravityManager>();
        characterController = GetComponent<CharacterController>();
        EstDansLesAirs = true;
    }

    void Start()
    {
        contactFilter.useTriggers = false;
        contactFilter.SetLayerMask(Physics2D.GetLayerCollisionMask(gameObject.layer));
        contactFilter.useLayerMask = true;
    }

    void FixedUpdate()
    {
        Vector2 deplacement = characterController.Deplacement;
        float distance = deplacement.magnitude;
        Debug.Log("deplacementcollidermanager : " + distance);

        if (distance > deplacementMinimal)
        {
            int count = rigidBody2D.Cast(deplacement, contactFilter, hitBuffer, distance + distanceSecurite);
            hitBufferList.Clear();
            for (int i = 0; i < count; i++)
            {
                hitBufferList.Add(hitBuffer[i]);
            }

            for (int i = 0; i < hitBufferList.Count; i++)
            {
                Vector2 currentNormal = hitBufferList[i].normal;
                Debug.Log("current normal : " + currentNormal);
                Debug.Log("distance hitbuffer : " + hitBufferList[i].distance);

                if (currentNormal.y == 1)
                    {
                        EstDansLesAirs = false;
                    }
                else
                    {
                        EstDansLesAirs = true;
                    }
                if (currentNormal.y == -1)
                    {
                        EstEnDessousDUnMur = true;
                    }
                else
                    {
                        EstEnDessousDUnMur = false;
                    }
                if (currentNormal.x == 1)
                    {
                        EstContreUnMurGauche = true;
                    }
                else
                    {
                        EstContreUnMurGauche = false;
                    }
                if (currentNormal.x == -1)
                    {
                        EstContreUnMurDroite = true;
                    }
                else
                    {
                        EstContreUnMurDroite = false;
                    }
                
            }


        }

    }

    public void DeplacerJoueur(Vector2 deplacement)
    {
        rigidBody2D.MovePosition(rigidBody2D.position + deplacement * Time.deltaTime);

    }
}
