﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class IntroductionInteractions : MonoBehaviour
{
    [SerializeField]
    GameObject controlsPage;

    private void Awake()
    {
        controlsPage.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene("LevelSelector");
        }

        if (Input.GetButtonDown("Dash"))
        {
            controlsPage.SetActive(true);
        }

        if (Input.GetButtonDown("Return"))
        {
            controlsPage.SetActive(false);
        }
    }
}
