﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatAnimation : MonoBehaviour
{
    public bool doubleDirection = false;
    public float speed = 2;
    public float distance = 2f;
    private Vector3 initPosition;

    void Awake()
    {
        speed += Random.Range(0f, speed) / 2;
        initPosition = transform.position;
    }

    private void Update()
    {
        if (!doubleDirection)
        {
            transform.position = initPosition + new Vector3(0, Mathf.Abs(Mathf.Sin(Time.time * speed)) * distance, 0);
        }
        else
        {
            transform.position = initPosition + new Vector3(0, Mathf.Sin(Time.time * speed) * distance, 0);
        }
    }

}
