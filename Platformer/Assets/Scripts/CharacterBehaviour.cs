﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBehaviour : MonoBehaviour
{
    public GameObject Startspawn;
    private Rigidbody2D rigidBody2D;

    void Awake()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        ReSpawn();
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReSpawn()
    {
        rigidBody2D.position = Startspawn.transform.position;
    }
    public void Death()
    {
        GetComponent<CharacterController>().PlaySoundDeath();
        ReSpawn();
    }

    void OnTriggerEnter(Collider collider)
    {
        ReSpawn();
    }

    void OnBecameInvisible()
    {
        Death();
    }

}
