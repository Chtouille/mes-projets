﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SelectionLevel : MonoBehaviour
{
    [SerializeField]
    GameObject selectionItem;

    GameObject selectBorder;

    [SerializeField]
    int nbOfLevels = 3;

    int oldAxis = 0;
    int levelCursor = 0;

    private void Awake()
    {
        GameObject temp = null;

        for (int i = 0; i < nbOfLevels; i++)
        {
            temp = Instantiate(selectionItem, new Vector3(- 284 + i * 104, -42, 0), Quaternion.identity);
            temp.transform.SetParent(gameObject.transform, false);
            temp.transform.GetChild(2).GetComponent<UnityEngine.UI.Text>().text = "Level " + i;
            temp.name = "Lvl" + i;
        }
        selectBorder = GameObject.Find("Lvl0").transform.GetChild(0).gameObject;
        selectBorder.SetActive(true);
    }

    private void Update()
    {
        float temp = Mathf.Floor(Input.GetAxis("Horizontal"));

        if ((temp < oldAxis) || (temp > oldAxis))
        {
            oldAxis = (int)Mathf.Sign(temp) * (int)Mathf.Ceil(Mathf.Abs(temp));

            if(oldAxis!=0)
            {
                if (levelCursor + oldAxis < 0)
                {
                    levelCursor = nbOfLevels + oldAxis;
                }
                else
                {
                    levelCursor += oldAxis;
                }

                selectBorder.SetActive(false);

                levelCursor = levelCursor % nbOfLevels;
                selectBorder = GameObject.Find("Lvl" + levelCursor).transform.GetChild(0).gameObject;

                Debug.Log(levelCursor);

                selectBorder.SetActive(true);

            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene("Level " + levelCursor);
        }

    }

}
