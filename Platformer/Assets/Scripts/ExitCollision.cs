﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ExitCollision : MonoBehaviour
{
    [SerializeField]
    private int nextLevel = 0;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Next level !");
        SceneManager.LoadScene("Level " + nextLevel);
        //if (player.GetComponent<PlayerController>().hasStarted)
        //{
        //   player.GetComponent<PlayerController>().isArrested = true;
        //}
    }
}
