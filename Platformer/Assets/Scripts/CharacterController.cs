﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    #region déclaration des variables

    private bool estDansLesAirs;
    public bool EstDansLesAirs { get => estDansLesAirs; set => estDansLesAirs = value; }

    private bool graviteFaible;
    public bool GraviteFaible { get => graviteFaible; set => graviteFaible = value; }

    private float vitesseGravite; 
    public float VitesseGravite { get => vitesseGravite; set => vitesseGravite = value; }

    private bool estEnTrainDeCourir;
    public bool EstEnTrainDeCourir { get => estEnTrainDeCourir; set => estEnTrainDeCourir = value; }

    public void PlaySoundJump()
    {
        audioSource.PlayOneShot(soundJump, 0.7F);
    }

    public void PlaySoundDeath()
    {
        audioSource.PlayOneShot(soundDeath, 0.7F);
    }


    private bool estContreUnMurGauche;
    public bool EstContreUnMurGauche { get => estContreUnMurGauche; set => estContreUnMurGauche = value; }

    private bool estContreUnMurDroite;
    public bool EstContreUnMurDroite { get => estContreUnMurDroite; set => estContreUnMurDroite = value; }

    //vrai si (EstDansLesAirs et déplacement suivant y négatif)
    private bool estEnTrainDeTomber;
    public bool EstEnTrainDeTomber { get => estEnTrainDeTomber; set => estEnTrainDeTomber = value; }

    private bool besoinRecalageGauche;
    public bool BesoinRecalageGauche { get => besoinRecalageGauche; set => besoinRecalageGauche = value; }

    private bool besoinRecalageDroite;
    public bool BesoinRecalageDroite { get => besoinRecalageDroite; set => besoinRecalageDroite = value; }

    private bool besoinRecalageHaut;
    public bool BesoinRecalageHaut { get => besoinRecalageHaut; set => besoinRecalageHaut = value; }

    private bool estEnTrainDeWallJump;
    public bool EstEnTrainDeWallJump { get => estEnTrainDeWallJump; set => estEnTrainDeWallJump = value; }

    private bool aWallJump;
    public bool AWallJump { get => aWallJump; set => aWallJump = value; }

    private Vector2 deplacement;
    public Vector2 Deplacement { get => deplacement; set => deplacement = value; }

    private Rigidbody2D rigidBody2D;
    private float inputDeplacementJoueur = 0f;
    private bool inputSaut = false;
    private bool inputDash = false;
    private float deplacementJoueurX = 0f;
    private float deplacementJoueurY = 0f;
    private float graviteValeur = 10.0f;
    private ContactFilter2D contactFilter;
    private RaycastHit2D[] hitBuffer = new RaycastHit2D[16];
    private List<RaycastHit2D> hitBufferList = new List<RaycastHit2D>(16);

    [SerializeField]
    private float graviteModifier = 1.0f;

    [SerializeField]
    private float GraviteFaibleModifier = 0.25f;

    [SerializeField]
    private float maxGravite = 2.0f;

    [SerializeField]
    private float maxGraviteFaible = 0.5f;

    [SerializeField]
    private float deplacementMinimal = 0.001f;

    [SerializeField]
    private float margeErreurCollision = 0.2f;

    [SerializeField]
    private float distanceSecurite = 0.5f;

    [SerializeField]
    private float distanceCollision = 0.01f;

    [SerializeField]
    private float distanceRecalage = 0.2f;

    [SerializeField]
    private float vitesseMaxMarche = 5f;

    [SerializeField]
    private float accelerationMarche = 15f;

    [SerializeField]
    private float decelerationMarche = 30f;

    [SerializeField]
    private float impulsionSautMarche = 19.0f;

    [SerializeField]
    private float vitesseMaxCourse = 10f;

    [SerializeField]
    private float accelerationCourse = 30f;

    [SerializeField]
    private float decelerationCourse = 40f;

    [SerializeField]
    private float impulsionSautCourse = 22.0f;

    [SerializeField]
    private float impulsionWallJumpMarche = 19.0f;

    [SerializeField]
    private float impulsionWallJumpCourse = 22.0f;

    [SerializeField]
    private float impulsionDash = 60.0f;

    [SerializeField]
    private float rebondMurVertical = 0.3f;

    #endregion

    #region Sounds

    [SerializeField]
    private AudioClip soundJump;

    [SerializeField]
    private AudioClip soundDeath;

    AudioSource audioSource;

    #endregion

    void Awake()
    {
        EstEnTrainDeCourir = false;
        EstContreUnMurGauche = false;
        EstContreUnMurDroite = false;
        EstEnTrainDeTomber = false;
        GraviteFaible = false;
        EstDansLesAirs = true;
        BesoinRecalageGauche = false;
        rigidBody2D = GetComponent<Rigidbody2D>();

        // Sounds
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!inputSaut)
        {
            inputSaut = Input.GetButtonDown("Jump");
        }

        if (!inputDash)
        {
            inputDash = Input.GetButtonDown("Dash");
        }

        if (Input.GetButtonDown("Return"))
        {
            SceneManager.LoadScene("LevelSelector");
        }

    }

    void FixedUpdate()
    {
        SetEtat();
        SetVitesseGravite(EstDansLesAirs, GraviteFaible);
        SetDeplacementJoueurX(inputDeplacementJoueur, deplacementJoueurX, EstEnTrainDeCourir);
        SetDeplacementJoueurY(VitesseGravite, inputSaut, deplacementJoueurY, EstDansLesAirs, EstEnTrainDeCourir);
        if (((EstContreUnMurDroite) || (EstContreUnMurGauche)) && (inputSaut == true) && EstDansLesAirs)
        {
            GestionWallJump(deplacementJoueurX, deplacementJoueurY);
        }
        Deplacement = new Vector2(deplacementJoueurX, deplacementJoueurY);

        if (inputDash)
        {
            Debug.Log("XmoveApresleSet " + deplacementJoueurX);
            Debug.Log("YmoveApresleSet " + deplacementJoueurY);
        }
        GestionCollisions(Deplacement);
        GestionRecalage(inputDeplacementJoueur);
        if (inputDash)
        {
            Debug.Log("XmoveApreslescoll " + deplacementJoueurX);
            Debug.Log("YmoveApreslescoll " + deplacementJoueurY);
        }
        DeplacerJoueur(Deplacement);
        inputSaut = false;
        inputDash = false;
    }

    void SetEtat()
    {
        inputDeplacementJoueur = Input.GetAxisRaw("Horizontal");

        EstEnTrainDeWallJump = false;

        int count0 = rigidBody2D.Cast(new Vector2 (0f,-1f), contactFilter, hitBuffer, distanceSecurite);
        hitBufferList.Clear();

        if (count0 == 0)
        {
            EstDansLesAirs = true;
        }

        int count1 = rigidBody2D.Cast(new Vector2(1f, 0f), contactFilter, hitBuffer, distanceSecurite);
        hitBufferList.Clear();

        if (count1 == 0)
        {
            EstContreUnMurDroite = false;
        }

        int count2 = rigidBody2D.Cast(new Vector2(-1f, 0f), contactFilter, hitBuffer, distanceSecurite);
        hitBufferList.Clear();

        if (count2 == 0)
        {
            EstContreUnMurGauche = false;
            Debug.Log("estcontreunmur gauche setetat : " + EstContreUnMurGauche);
        }

        if (deplacementJoueurY != 0f)
        {
            EstDansLesAirs = true;
        }

        if ((EstDansLesAirs) && (deplacementJoueurY <= 0f))
        {
            EstEnTrainDeTomber = true;
        }
        else
        {
            EstEnTrainDeTomber = false;
        }

        if (((EstContreUnMurGauche) || (EstContreUnMurDroite)) && (EstEnTrainDeTomber))
        {
            GraviteFaible = true;
        }
        else
        {
            GraviteFaible = false;
        }

        if (Input.GetAxisRaw("Course") == 1)
        {
            EstEnTrainDeCourir = true;
        }
        else
        {
            EstEnTrainDeCourir = false;
        }

    }

    private void SetVitesseGravite(bool estDansLesAirs, bool graviteFaible)
    {
        if (estDansLesAirs)
        {
            if (graviteFaible)
            {
                graviteValeur = GraviteFaibleModifier;
                //La vitesse de chute augmente jusqu'à une valeur limite
                if (Mathf.Abs(VitesseGravite) <= maxGraviteFaible)
                {
                    VitesseGravite += graviteValeur * Physics2D.gravity.y * Time.deltaTime;
                }
                else
                {
                    VitesseGravite = - maxGraviteFaible;
                }
            }
            else
            {
                graviteValeur = graviteModifier;
                //La vitesse de chute augmente jusqu'à une valeur limite
                if (Mathf.Abs(VitesseGravite) <= maxGravite)
                {
                    VitesseGravite += graviteValeur * Physics2D.gravity.y * Time.deltaTime;
                }
            }
        }

        else
        {
            graviteValeur = 0f;
            VitesseGravite = 0f;
        }
    }

    void GestionRecalage(float inputJoueur)
    {
        if (((inputJoueur > 0f) || (EstEnTrainDeWallJump)) && (BesoinRecalageGauche))
        {
            Deplacement = new Vector2(Deplacement.x + (distanceRecalage / Time.deltaTime), Deplacement.y);
            BesoinRecalageGauche = false;
        }

        if (((inputJoueur < 0f) || (EstEnTrainDeWallJump)) && (BesoinRecalageDroite))
        {
            Deplacement = new Vector2(Deplacement.x - (distanceRecalage / Time.deltaTime), Deplacement.y);
            BesoinRecalageDroite = false;
        }

        if (BesoinRecalageHaut)
        {
            Deplacement = new Vector2(Deplacement.x , Deplacement.y - (distanceRecalage / Time.deltaTime));
            BesoinRecalageHaut = false;

        }
    }

    void GestionCollisions(Vector2 deplacement)
    {
        float _deplacementJoueurY = deplacement.y;
        float _deplacementJoueurX = deplacement.x;
        float distance = deplacement.magnitude;
        if (distance > deplacementMinimal)
        {
            int count = rigidBody2D.Cast(deplacement, contactFilter, hitBuffer, distance + margeErreurCollision);
            hitBufferList.Clear();

            for (int i = 0; i < count; i++)
            {
                if(hitBuffer[i].collider.tag=="Exit") {
                    continue;
                }
                if (hitBuffer[i].collider.tag == "Laser")
                {
                    Debug.Log("Laser");
                    continue;
                    //if (inputDash)
                    //{
                    //    Debug.Log("No laser");
                    //}
                    //else
                    //{
                    //   continue;
                    //}
                }
                hitBufferList.Add(hitBuffer[i]);
            }

            for (int i = 0; i < hitBufferList.Count; i++)
            {
                if (hitBufferList[i].distance <= distanceSecurite)
                {
                    Vector2 currentNormal = hitBufferList[i].normal;

                    if ((currentNormal.y == 1) && (hitBufferList[i].distance == 0f))
                    {
                        deplacementJoueurY = _deplacementJoueurY;
                        _deplacementJoueurY = distanceRecalage / Time.deltaTime;                       
                    }

                    if ((currentNormal.y == 1) && (EstDansLesAirs))
                    {
                        _deplacementJoueurY = (-hitBufferList[i].distance + distanceCollision)/Time.deltaTime;
                        deplacementJoueurY = 0f;
                        EstDansLesAirs = false;
                    }

                    if ((currentNormal.y == -1) && (hitBufferList[i].distance <= 3 * distanceCollision))
                    {
                        _deplacementJoueurY = _deplacementJoueurY * -1 * rebondMurVertical;
                        deplacementJoueurY = _deplacementJoueurY;

                        if (hitBufferList[i].distance == 0f)
                        {
                            BesoinRecalageHaut = true;
                        }
                    }

                    if (currentNormal.x == 1)
                    {
                        if ((hitBufferList[i].distance == 0f))
                        {
                            BesoinRecalageGauche = true;
                        }

                        if (EstContreUnMurGauche == false)
                        {
                            EstContreUnMurGauche = true;
                            if ((deplacement.x < 0) && (!EstEnTrainDeWallJump))
                            {
                                _deplacementJoueurX = (-hitBufferList[i].distance + distanceCollision) / Time.deltaTime;
                                deplacementJoueurX = 0f;
                            }
                        }
                        else if (!EstEnTrainDeWallJump) 
                        {
                            _deplacementJoueurX = 0f;
                            deplacementJoueurX = 0f;
                        }

                    }

                    if (currentNormal.x == -1)
                    {
                        if ((hitBufferList[i].distance == 0f))
                        {
                            BesoinRecalageDroite = true;
                        }

                        if (EstContreUnMurDroite == false)
                        {
                                EstContreUnMurDroite = true;
                                if ((deplacement.x > 0) && (!EstEnTrainDeWallJump))
                                {
                                    _deplacementJoueurX = (hitBufferList[i].distance - distanceCollision)/Time.deltaTime;
                                    deplacementJoueurX = 0f;
                                }
                        }
                        else if (!EstEnTrainDeWallJump)
                        {
                                _deplacementJoueurX = 0f;
                                deplacementJoueurX = 0f;
                        }

                        
                    }
                    
                }

            }
        }

        if ((_deplacementJoueurX < 0f) && (EstContreUnMurDroite == true))
        {
            EstContreUnMurDroite = false;
        }

        if ((_deplacementJoueurX > 0f) && (EstContreUnMurGauche == true))
        {
            EstContreUnMurGauche = false;
        }

        Deplacement = new Vector2(_deplacementJoueurX, _deplacementJoueurY);
    }

    void GestionWallJump(float _deplacementJoueurX, float _deplacementJoueurY)
    {
        float impulsionWallJump = impulsionWallJumpMarche;

        if (EstEnTrainDeCourir)
        {
            impulsionWallJump = impulsionWallJumpCourse;
        }

        if ((EstContreUnMurGauche)&&(!BesoinRecalageGauche))
        {
            deplacementJoueurX = impulsionWallJump;
        }

        if ((EstContreUnMurDroite) && (!BesoinRecalageDroite))
        {
            deplacementJoueurX = - impulsionWallJump;
        }

        deplacementJoueurY = impulsionWallJump;
        EstEnTrainDeWallJump = true;

        // Jump sound
        PlaySoundJump();
    }

    //prend un compte le déplacement à la frame d'avant, les inputs courants et établis le déplacement courant
    void SetDeplacementJoueurX(float inputDeplacementJoueur, float _deplacementJoueurX, bool estEnTrainDeCourir)
    {
        float vitesseMax = vitesseMaxMarche;
        float acceleration = accelerationMarche;
        float deceleration = decelerationMarche;

        if (estEnTrainDeCourir)
        {
            vitesseMax = vitesseMaxCourse;
            acceleration = accelerationCourse;
            deceleration = decelerationCourse;
        }

        if ((_deplacementJoueurX == 0f) && (inputDeplacementJoueur == 0f))
        {
            deplacementJoueurX = 0f;
        }

        if (inputDash)
        {
            if (inputDeplacementJoueur < 0f)
            {
                deplacementJoueurX = -impulsionDash;
            }

            else if (inputDeplacementJoueur > 0f)
            {
                deplacementJoueurX = impulsionDash;
            }
        }

        else if (inputDeplacementJoueur != 0f)
        {
            if (Mathf.Abs(_deplacementJoueurX) <= vitesseMax)
            {
                deplacementJoueurX = _deplacementJoueurX + inputDeplacementJoueur * acceleration * Time.deltaTime;
            }
            else
            {
                deplacementJoueurX = Mathf.Sign(_deplacementJoueurX) * vitesseMax;
            }
        }

        else if (inputDeplacementJoueur == 0f)
        {
            deplacementJoueurX = _deplacementJoueurX - Mathf.Sign(deplacementJoueurX) * deceleration * Time.deltaTime;
            if (Mathf.Sign(_deplacementJoueurX) != Mathf.Sign(deplacementJoueurX))
            {
                deplacementJoueurX = 0f;
            }
        }


        if ((inputDeplacementJoueur < 0f) && EstContreUnMurGauche)
        {
            deplacementJoueurX = 0f;
        }

        if ((inputDeplacementJoueur > 0f) && EstContreUnMurDroite)
        {
            deplacementJoueurX = 0f;
        }
    }

    void SetDeplacementJoueurY(float vitesseGravite, bool inputSaut, float _deplacementJoueurY, bool estDansLesAirs, bool estEnTrainDeCourir)
    {
        float impulsionSaut = impulsionSautMarche;

        if (estEnTrainDeCourir)
        {
            impulsionSaut = impulsionSautCourse;
        }

        if ((inputSaut == false) && (!estDansLesAirs))
        {
            deplacementJoueurY = 0f;
        }

        else if ((inputSaut == true) && (!estDansLesAirs))
        {
            deplacementJoueurY = impulsionSaut;

            // Jump sound
            audioSource.PlayOneShot(soundJump, 0.7F);
        }

        else if (estDansLesAirs)
        {
            deplacementJoueurY = _deplacementJoueurY + vitesseGravite;
        }

        if (inputDash)
        {
            deplacementJoueurY = 0f;
        }

    }

    public void DeplacerJoueur(Vector2 deplacement)
    {
        rigidBody2D.position = new Vector2 ((rigidBody2D.position + deplacement * Time.deltaTime).x, (rigidBody2D.position + deplacement * Time.deltaTime).y) ;
    }
}
