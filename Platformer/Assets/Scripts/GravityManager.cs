﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityManager : MonoBehaviour
{
    #region déclaration des variables

    //graviteFaible permet de gérer les frottements contre les murs
    private bool graviteFaible;
    public bool GraviteFaible{ get => graviteFaible; set => graviteFaible = value; }

    private bool estDansLesAirs;
    public bool EstDansLesAirs { get => estDansLesAirs; set => estDansLesAirs = value; }

    private float vitesseGravite { get; set; }
    public float VitesseGravite { get => vitesseGravite; set => vitesseGravite = value; }

    private Rigidbody2D rigidBody2D;
    private CharacterController characterController;
    private ColliderManager colliderManager;
    private float graviteValeur = 10.0f;

    [SerializeField]
    private float graviteModifier = 10.0f;

    [SerializeField]
    private float GraviteFaibleModifier = 3.0f;

    [SerializeField]
    private float maxGravite = 20.0f;
    #endregion

    void Awake()
    {
        GraviteFaible = false;
        EstDansLesAirs = true;
        rigidBody2D = GetComponent<Rigidbody2D>();
        characterController = GetComponent<CharacterController>();
        colliderManager = GetComponent<ColliderManager>();
    }

    void FixedUpdate()
    {
        SetEtat();
        SetVitesseGravite(EstDansLesAirs, GraviteFaible);
    }

    private void SetEtat()
    {
        EstDansLesAirs = characterController.EstDansLesAirs;
        if ((characterController.EstEnTrainDeTomber)&&((characterController.EstContreUnMurGauche)||(characterController.EstContreUnMurDroite)))
        {
            GraviteFaible = true;
        }
        else
        {
            GraviteFaible = false;
        }
    }

    private void SetVitesseGravite(bool estDansLesAirs, bool graviteFaible)
    {
        if (estDansLesAirs)
        {
            if (graviteFaible)
            {
                graviteValeur = GraviteFaibleModifier;
                //La vitesse de chute augmente jusqu'à une valeur limite
                if (Mathf.Abs(VitesseGravite) >= maxGravite)
                {
                    VitesseGravite += graviteValeur * Physics2D.gravity.y * Time.deltaTime;
                }
            }
            else
            {
                graviteValeur = graviteModifier;
                //La vitesse de chute augmente jusqu'à une valeur limite
                if (Mathf.Abs(VitesseGravite) <= maxGravite)
                {
                    VitesseGravite += graviteValeur * Physics2D.gravity.y * Time.deltaTime;
                }
            }
        }

        else
        {
            graviteValeur = 0f;
            VitesseGravite = 0f;
        }
    }
}
